﻿namespace CRM.Entity
{
    public class BaseEntity<T>
    {
        public T Id { get; set; }
    }
}