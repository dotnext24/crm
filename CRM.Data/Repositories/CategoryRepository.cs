﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CRM.Data.DBContexts;
using CRM.Data.Infrastructure;
using CRM.Data.Repositories.Interface;
using CRM.Entity;
using CRM.Entity.Custom;

namespace CRM.Data.Repositories
{
    public class CategoryRepository : RepositoryBase<Category>, ICategoryRepository
    {
        private readonly FirstDbContext _dbContext;

        public CategoryRepository(DbContext dbContext)
            : base(dbContext)
        {
            _dbContext = (_dbContext ?? (FirstDbContext) dbContext);
        }

        public IEnumerable<CategoryWithExpense> GetCategoryWithExpenses()
        {
            var category = GetAll().Join(_dbContext.Expenses, c => c.Id, e => e.CategoryId, (c, e) => new {c, e})
                .GroupBy(z => new {z.c.Id, z.c.Name, z.c.Description}, z => z.e)
                .Select(
                    g =>
                        new CategoryWithExpense
                        {
                            CategoryId = g.Key.Id,
                            CategoryName = g.Key.Name,
                            Description = g.Key.Description,
                            TotalExpenses = g.Sum(s => s.Amount)
                        });
            return category;
        }
    }
}