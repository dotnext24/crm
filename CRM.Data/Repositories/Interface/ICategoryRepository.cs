﻿using System.Collections.Generic;
using CRM.Data.Infrastructure;
using CRM.Entity;
using CRM.Entity.Custom;

namespace CRM.Data.Repositories.Interface
{
    public interface ICategoryRepository : IRepository<Category>
    {
        IEnumerable<CategoryWithExpense> GetCategoryWithExpenses();
    }
}