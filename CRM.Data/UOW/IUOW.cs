﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using CRM.Data.Infrastructure;
using CRM.Data.Repositories.Interface;
using CRM.Entity;

namespace CRM.Data.UnitOfWork
{
    public interface IUOW<U> where U : DbContext, IDisposable
    {
        int Commit();
        Task<int> CommitAsync();

        /// <summary>
        /// Repository intefaces
        /// </summary>
        ICategoryRepository CategoryRepository { get; }

        IRepository<Department> DepartmentRepository { get; }
    }
}