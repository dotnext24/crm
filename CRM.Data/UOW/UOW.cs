﻿using System.Data.Entity;
using System.Threading.Tasks;
using CRM.Data.Infrastructure;
using CRM.Data.Repositories;
using CRM.Data.Repositories.Interface;
using CRM.Entity;

namespace CRM.Data.UnitOfWork
{
    public class UOW<TContext> : Disposable, IUOW<TContext>
        where TContext : DbContext, new()
    {
        public virtual int Commit()
        {
            return _dataContext.SaveChanges();
        }

        public virtual Task<int> CommitAsync()
        {
            return _dataContext.SaveChangesAsync();
        }

        private readonly DbContext _dataContext;

        public UOW()
        {
            _dataContext = new TContext();
        }

        protected override void DisposeCore()
        {
            if (_dataContext != null)
                _dataContext.Dispose();
        }

        /// <summary>
        /// Define Repositories
        /// </summary>
        private ICategoryRepository _categoryRepository;

        public ICategoryRepository CategoryRepository
        {
            get { return _categoryRepository ?? (_categoryRepository = new CategoryRepository(_dataContext)); }
        }

        private IRepository<Department> _departmentRepository;

        public IRepository<Department> DepartmentRepository
        {
            get
            {
                return _departmentRepository ?? (_departmentRepository = new RepositoryBase<Department>(_dataContext));
            }
        }
    }
}