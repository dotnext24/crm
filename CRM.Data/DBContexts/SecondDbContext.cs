﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using CRM.Entity;

namespace CRM.Data.DBContexts
{
    public class SecondDbContext : DbContext
    {
        static SecondDbContext()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<SecondDbContext>());
        }

        public IDbSet<Department> Departments { get; set; }
        public IDbSet<Employee> Employees { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}