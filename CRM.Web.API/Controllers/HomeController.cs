﻿using System.Web.Mvc;

namespace CRM.Web.API.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }
    }
}