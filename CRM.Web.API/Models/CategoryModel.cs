﻿namespace CRM.Web.API.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }
        public virtual string Name { get; set; }
    }
}